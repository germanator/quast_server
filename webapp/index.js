const mysql = require('mysql');
const express = require('express')
const app = express()
const port = 3000
console.log("TESTING!!!");


let connStatus = false;
let connection = null

connection = mysql.createConnection({
    host     : 'db',
    database : 'data',
    port     : 3306,
    user     : 'root',
    password : 'abc123'
});

connection.connect();

const getEmployees = async => {
    const query = "SELECT * FROM employees LIMIT 100;";
    return new Promise((resolve, reject) => {
        connection.query(query, (err, rows) => {
            if(rows === undefined){
                reject(new Error("Error!"));
            }else{
                resolve(rows);
            }
        })
    })
}

app.get('/getEmployees', async (req, res) => {
    const rows = await getEmployees();
    let htmlBuilder = "<html><table><tr><th>Employee ID</th><th>Birthdate</th><th>First Name</th><th>Last Name</th><th>Gender</th><th>Hire Date</th></tr>"
    rows.forEach(employee => {
        htmlBuilder += `<tr><th>${employee.emp_no}</th><th>${employee.birth_date}</th><th>${employee.first_name}</th><th>${employee.last_name}</th><th>${employee.gender}</th><th>${employee.hire_date}</th></tr>`
    });
    htmlBuilder += "</table></html>"
    res.send(htmlBuilder)
})
app.get('/', (req, res) => {
    res.send("<html><h2>Welcome!</h2>Click <a href='/getEmployees'>here</a> to get a list of all employees</html>")
})

app.listen(port, () => {
    console.log(`DB app listening at http://localhost:${port}`)
})