# Instructions
This demo shows how to launch a crude mysql server, populate it with data and launch a node app that can retrieve data

## Requirements
To run this demo, you'll need:

1. docker
2. docker-compose

## UPDATES:
*3/3/2021:*
 - Split all services into seperate folders, and created individual Dockerfiles for each. This seperates logic
 - Added new "cron" component. This triggers every 60 seconds and runs a ```SELECT * FROM employees LIMIT 10;``` against the database. To view and make sure it's working, inspect the ```/var/log/mysql-cron.log``` file in the cron container
 - Added draft of script to inject new data into employees table. Meant to mimic regular web scrape and insert into DB


## Instructions:
On first build, run 
```bash
docker-compose -f docker-compose.yml up -d
```
On first run, it may take some time to set up, but should spin up. To monitor the process, check ```docker ps``` regularly.

You should now be able to access http://localhost in your web browser, and when you click the link, it should show you the first 100 rows of the *massive* dataset in ```data.sql```

TO shut the servcie down, run
```bash
docker-compose -f docker-compose.yml down
```

*NOTE: On subsequent runs, changes in the node app won't be reflected unless you run ```docker-compose build```.*